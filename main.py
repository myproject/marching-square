import random
import time

import pygame as pygame

from Square import Square

#constant
WINDOWSIZE = (1000, 1000)
nbDoteByLine = 100
showLines = True
showDotes = True
distBetweenDote = (WINDOWSIZE[0] / nbDoteByLine, WINDOWSIZE[1] / nbDoteByLine)

pygame.init()
fenetre = pygame.display.set_mode(WINDOWSIZE)
pygame.display.set_caption("marching square")

time_per_frame = 1 / 60
last_time = time.time()
frameCount = 0

allDotes = []
allSquares = []
def setup():
    global allDotes
    global allSquares
    allDotes = []
    allSquares = []

    for y in range(0, nbDoteByLine):
        allDotes.append([])
        for x in range(0, nbDoteByLine):
            allDotes[y].append(random.randint(0, 1))

    for y in range(0, len(allDotes)):
        for x in range(0, len(allDotes[y])):
            if x != len(allDotes[y]) - 1 and y != len(allDotes) - 1:
                hg = allDotes[y][x]
                hd = allDotes[y][x+1]
                bg = allDotes[y+1][x]
                bd = allDotes[y+1][x+1]
                allSquares.append(Square(hg, hd, bg, bd, (x, y)))

setup()

run = True
while run:
    delta_time = time.time() - last_time
    while delta_time < time_per_frame:
        delta_time = time.time() - last_time

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False

            keys = pygame.key.get_pressed()
            if keys[pygame.K_ESCAPE]:
                run = False
            elif keys[pygame.K_F5]:
                setup()

        fenetre.fill((0, 0, 0))
        if showDotes:
            for y in range(0, len(allDotes)):
                for x in range(0, len(allDotes[y])):
                    if allDotes[y][x] == 0:
                        color = (0, 0, 0)
                    else:
                        color = (255, 255, 255)
                    pygame.draw.circle(fenetre, color, (x * distBetweenDote[0], y * distBetweenDote[1]), 1)

        if showLines:
            for square in allSquares:
                square.draw(fenetre, distBetweenDote)

        pygame.display.update()
    last_time = time.time()
    frameCount += 1
