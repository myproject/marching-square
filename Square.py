import pygame.draw


class Square:

    def __init__(self, hg, hd, bg, bd, hgPos):
        self.square = [[hg, hd], [bg, bd]]
        self.hgPos = hgPos

    # (self.hgPos[0] * distBetweenDote[0], self.hgPos[1] * distBetweenDote[1])
    def getPoint(self, distBetweenDote):
        res = None
        if self.square[0][0] == self.square[0][1] and  self.square[0][1] == self.square[1][0] and self.square[1][0] == self.square[1][1]:
            res = ()
        else:
            if self.square[0][0] == 1 and self.square[1][0] == 0 and self.square[0][1] == 0 and self.square[1][1] == 0:
                p1 = (self.hgPos[0] * distBetweenDote[0] + (distBetweenDote[0] / 2), self.hgPos[1] * distBetweenDote[1])
                p2 = (self.hgPos[0] * distBetweenDote[0], self.hgPos[1] * distBetweenDote[1] + (distBetweenDote[1] / 2))
                res = ([(p1, p2)])
            elif self.square[0][0] == 0 and self.square[1][0] == 1 and self.square[0][1] == 0 and self.square[1][1] == 0:
                p1 = (self.hgPos[0] * distBetweenDote[0], self.hgPos[1] * distBetweenDote[1] + (distBetweenDote[1] / 2))
                p2 = (self.hgPos[0] * distBetweenDote[0] + (distBetweenDote[0] / 2), (self.hgPos[1] + 1) * distBetweenDote[1])
                res = ([(p1, p2)])
            elif self.square[0][0] == 0 and self.square[1][0] == 0 and self.square[0][1] == 1 and self.square[1][1] == 0:
                p1 = (self.hgPos[0] * distBetweenDote[0] + (distBetweenDote[0] / 2), self.hgPos[1] * distBetweenDote[1])
                p2 = ((self.hgPos[0] + 1) * distBetweenDote[0], self.hgPos[1] * distBetweenDote[1] + + (distBetweenDote[1] / 2))
                res = ([(p1, p2)])
            elif self.square[0][0] == 0 and self.square[1][0] == 0 and self.square[0][1] == 0 and self.square[1][1] == 1:
                p1 = ((self.hgPos[0] + 1) * distBetweenDote[0], self.hgPos[1] * distBetweenDote[1] + + (distBetweenDote[1] / 2))
                p2 = (self.hgPos[0] * distBetweenDote[0] + (distBetweenDote[0] / 2), (self.hgPos[1] + 1) * distBetweenDote[1])
                res = ([(p1, p2)])
            elif (self.square[0][0] == 1 and self.square[1][0] == 0 and self.square[0][1] == 1 and self.square[1][1] == 0) or (self.square[0][0] == 0 and self.square[1][0] == 1 and self.square[0][1] == 0 and self.square[1][1] == 1):
                p1 = (self.hgPos[0] * distBetweenDote[0], self.hgPos[1] * distBetweenDote[1] + (distBetweenDote[1] / 2))
                p2 = ((self.hgPos[0] + 1) * distBetweenDote[0], self.hgPos[1] * distBetweenDote[1] + (distBetweenDote[1] / 2))
                res = ([(p1, p2)])
            elif self.square[0][0] == 1 and self.square[1][0] == 0 and self.square[0][1] == 0 and self.square[1][1] == 1:
                p1 = (self.hgPos[0] * distBetweenDote[0], self.hgPos[1] * distBetweenDote[1] + (distBetweenDote[1] / 2))
                p2 = (self.hgPos[0] * distBetweenDote[0] + (distBetweenDote[0] / 2), (self.hgPos[1] + 1) * distBetweenDote[1])
                p3 = (self.hgPos[0] * distBetweenDote[0] + (distBetweenDote[0] / 2), self.hgPos[1] * distBetweenDote[1])
                p4 = ((self.hgPos[0] + 1) * distBetweenDote[0], self.hgPos[1] * distBetweenDote[1] + (distBetweenDote[1] / 2))
                res = ([(p1, p2), (p3, p4)])
            elif (self.square[0][0] == 1 and self.square[1][0] == 1 and self.square[0][1] == 0 and self.square[1][1] == 0) or (self.square[0][0] == 0 and self.square[1][0] == 0 and self.square[0][1] == 1 and self.square[1][1] == 1):
                p1 = (self.hgPos[0] * distBetweenDote[0] + (distBetweenDote[0] / 2), self.hgPos[1] * distBetweenDote[1])
                p2 = (self.hgPos[0] * distBetweenDote[0] + (distBetweenDote[0] / 2), (self.hgPos[1] + 1) * distBetweenDote[1])
                res = ([(p1, p2)])
            elif self.square[0][0] == 0 and self.square[1][0] == 1 and self.square[0][1] == 1 and self.square[1][1] == 0:
                p1 = (self.hgPos[0] * distBetweenDote[0] + (distBetweenDote[0] / 2), self.hgPos[1] * distBetweenDote[1])
                p2 = ((self.hgPos[0] + 1) * distBetweenDote[0], self.hgPos[1] * distBetweenDote[1] + (distBetweenDote[1] / 2))
                p3 = (self.hgPos[0] * distBetweenDote[0], self.hgPos[1] * distBetweenDote[1] + (distBetweenDote[1] / 2))
                p4 = (self.hgPos[0] * distBetweenDote[0] + (distBetweenDote[0] / 2), (self.hgPos[1] + 1) * distBetweenDote[1])
                res = ([(p1, p2), (p3, p4)])
            elif self.square[0][0] == 1 and self.square[1][0] == 0 and self.square[0][1] == 1 and self.square[1][1] == 1:
                p1 = (self.hgPos[0] * distBetweenDote[0], self.hgPos[1] * distBetweenDote[1] + (distBetweenDote[1] / 2))
                p2 = (self.hgPos[0] * distBetweenDote[0] + (distBetweenDote[0] / 2),  (self.hgPos[1] + 1) * distBetweenDote[1])
                res = ([(p1, p2)])
            elif self.square[0][0] == 1 and self.square[1][0] == 1 and self.square[0][1] == 1 and self.square[1][1] == 0:
                p1 = ((self.hgPos[0] + 1) * distBetweenDote[0], self.hgPos[1] * distBetweenDote[1] + (distBetweenDote[1] / 2))
                p2 = (self.hgPos[0] * distBetweenDote[0] + (distBetweenDote[0] / 2),  (self.hgPos[1] + 1) * distBetweenDote[1])
                res = ([(p1, p2)])
            elif self.square[0][0] == 1 and self.square[1][0] == 1 and self.square[0][1] == 0 and self.square[1][1] == 1:
                p1 = (self.hgPos[0] * distBetweenDote[0] + (distBetweenDote[0] / 2), self.hgPos[1] * distBetweenDote[1])
                p2 = ((self.hgPos[0] + 1) * distBetweenDote[0], self.hgPos[1] * distBetweenDote[1] + (distBetweenDote[1] / 2))
                res = ([(p1, p2)])
            elif self.square[0][0] == 0 and self.square[1][0] == 1 and self.square[0][1] == 1 and self.square[1][1] == 1:
                p1 = (self.hgPos[0] * distBetweenDote[0] + (distBetweenDote[0] / 2), self.hgPos[1] * distBetweenDote[1])
                p2 = (self.hgPos[0] * distBetweenDote[0], self.hgPos[1] * distBetweenDote[1] + (distBetweenDote[1] / 2))
                res = ([(p1, p2)])

        return res

    def draw(self, fenetre, distBetweenDote):
        points = self.getPoint(distBetweenDote)
        if points != ():
            for p in points:
                pygame.draw.line(fenetre, (255, 255, 255), p[0], p[1], 1)